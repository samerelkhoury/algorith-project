#!/usr/bin/env python3
# -*- coding: utf-8 -*
""" Architecture of the interface ."""

__author__ = 'Samer El Khoury'

import tkinter as tk
import os
from tkinter import ttk, filedialog, messagebox
import ttkthemes as themes
from b_w_t import Bwt
from huffman import Huffman_creator


class Interface(themes.ThemedTk):
    """
    Class constructor

    Returns
    -------
    None

    """
    def __init__(self):
        super().__init__()
        self.theme = themes.ThemedStyle(self)
        self.theme.set_theme("radiance")
        self.frame = ttk.Frame(self)
        self.frame.grid(column=0,row=1,columnspan=2)
        self.scrollbar= tk.Scrollbar(self.frame)
        self.scrollbar.pack(side="right", fill="y")
        self.text = tk.Text(self.frame, width=80, height=29)
        self.text['yscrollcommand'] =self.scrollbar.set
        self.text.pack(padx= 15)
        self.scrollbar.config(command=self.text.yview)

        self.config(background="#F6F6F5")
        self.title("Burrow Wheeler Transform & Huffman")
        self.geometry("700x1000")

        self.file = None                    #intialize the variables
        self.seq = None
        self.reversed_dictionnary = None
        self.ascii_code = None
        self.bwt_creator = None        #used later to stock the sequences that are being processed using bwt or huffman
 
        #creating each button with it's name,position and function
        self.open_button = ttk.Button(self, text='open', command=self.open_file)
        self.open_button.grid(column=0, row=0, padx=20)

        self.save_button = ttk.Button(self, text='Save', command=self.call_save)
        self.save_button.grid(column=1, row=0, padx=10)
        
        self.bwt_button = ttk.Button(self, text='BWT Sequence', command=self.call_bwt_creator)
        self.bwt_button.grid(column=0, row=2, pady=5)

        self.compress_button = ttk.Button(self, text='Compress', command=self.call_huffman_compression)
        self.compress_button.grid(column=1, row=2, pady=5)
        
        self.reverse_button = ttk.Button(self, text='Reverse BWT', command=self.call_reverse_bwt)
        self.reverse_button.grid(column=0, row=3, pady=5)

        self.next_button = ttk.Button(self, text='Next', command=self.call_next_button)
        self.next_button.grid(column=0, row=4, pady=25)

        self.decompress_button = ttk.Button(self, text='Decompress', command=self.call_huffman_decompression)
        self.decompress_button.grid(column=1, row=3, pady=5)

        self.preview_button = ttk.Button(self, text='Preview', command= self.call_preview_button)
        self.preview_button.grid(column=1, row=4, pady=25)

    def open_file(self):
        """ 

        Method to select a DNA sequence from your computer

        self.seq:
                The chosen DNA sequence
        """
        self.file = filedialog.askopenfilename(
            initialdir= os.getcwd(),title="Select File",filetypes=(
                ("Text Files", "*.txt"),("all files","*.*")))

        if self.file:
            messagebox.showinfo("Selected file", "You have selected %s"%(
                self.file))
            with open(self.file) as file :
                self.seq = file.read()
                self.seq = self.seq.replace("\n","")    #corrects the bad spacing inside the sequence
                self.seq = self.seq.replace(" ","")

                seq_separator = self.seq.split('{')     #reconverting a string dictionnary to a real dictionnary
                self.seq = seq_separator[0]             # by decomposing it 
                self.ascii_code = self.seq              #useful when decompressing a compressed file
                if len(seq_separator) > 1 :
                    seq_separator[1] = seq_separator[1][:-1]
                    seq_separator[1] = seq_separator[1].replace("'","")
                    dict_items = seq_separator[1].split(',')
                    self.reversed_dictionnary = {}
                    for i in dict_items :
                        temp_items = i.split(':')
                        self.reversed_dictionnary[temp_items[0]] = temp_items[1]

                self.text.delete(1.0,"end")                                    #deletes everything inside the text widget
                self.text.insert("end","The chosen DNA sequence: "+self.seq)   #inserts inside the text widget
        self.bwt_button['state'] = 'normal'                                    #allows the buttons to be clickable
        self.compress_button['state'] = 'normal'
        self.reverse_button['state'] = 'normal'
        self.decompress_button['state'] = 'normal'
                

    def call_bwt_creator(self):
        """

        Method that creates a Bwt object and uses the bwt_creator method; used as the function of the BWT sequence button

        """
        if "$" in self.seq:
            messagebox.showwarning('Warning !',"The sequence is invalid")    #checks if the sequence is valide; doesn't contain a $
        else :
            BWT_object1 = Bwt(self.seq)
            self.bwt_creator = BWT_object1.bwt_creator(self.seq)
            self.text.delete(1.0, "end")
            self.text.insert("end", "You have chosen the The Burrows-Wheeler Transformation"+"\n"+" Please click on the Next Button or Preview Button") 

    def call_reverse_bwt(self):
        """

        Method that creates a Bwt object and uses the reverse_bwt method; used as the function of the Reverse BWT button

        """
        if "$" not in self.seq:
            messagebox.showwarning('Warning !',"The sequence is invalid")   #checks if the sequence is valide
        else:
            BWT_object2 = Bwt(self.seq)
            self.bwt_creator = BWT_object2.reverse_bwt(self.seq)
            self.text.delete(1.0, "end")
            self.text.insert("end", "You have chosen the The Huffman Transformation"+"\n"+" Please click on the Next Button or Preview Button")

    def call_huffman_compression(self):
        """

        Method that creates a Huffman object and uses the encode_huffman method and the binary_to_ASCII; used as the function of the compress button
        in order to get the binary sequence of self.seq the main DNA sequence and the ascii code 
        main DNA sequence > binary version > ascii version 

        """
        huffman_sequence = Huffman_creator()                 #calling the Huffman creator class
        binary_sequence, reversed_dict,coding_dict = huffman_sequence.encode_huffman(self.seq)  
        self.text.delete(1.0, "end")
        self.text.insert("end", "Binary sequence: " + binary_sequence)
        binary_to_ascii, reversed_dict = huffman_sequence.binary_to_ASCII(binary_sequence, reversed_dict)
        self.text.insert("end", "\n"+ "Compressed sequence: " + binary_to_ascii)
        self.compress_button['state'] = 'disabled'
        self.bwt_button['state'] = 'disabled'                             # The state of the buttons is disabled depending on the case
        self.decompress_button['state'] = 'normal'
        self.reverse_button['state'] = 'disabled'  
        self.ascii_code = binary_to_ascii                                #stocking the results in the variables initiated at the beginning
        self.reversed_dictionnary = reversed_dict
        self.seq = self.ascii_code


    def call_huffman_decompression(self):
        """

        Method that creates a Huffman object and uses the ASCII_to_binary method and the binary_to_original_sequence; used as the function of the decompress button
        in order to get the main sequence back but we need to pass through the binary version first 
        ascii version > binary version > main DNA sequence 

        """
        huffman_sequence = Huffman_creator
        back_to_binary = huffman_sequence.ASCII_to_binary(self.ascii_code, self.reversed_dictionnary)
        self.text.delete(1.0, "end")
        self.text.insert("end", "Binary sequence: " + back_to_binary)
        original_sequence = huffman_sequence.binary_to_original_sequence(back_to_binary, self.reversed_dictionnary)
        print(original_sequence)
        self.text.insert("end", "\n" + "Original sequence: " + original_sequence)
        self.decompress_button['state'] = 'disabled'
        self.bwt_button['state'] = 'normal'
        self.compress_button['state'] = 'normal'
        self.reverse_button['state'] = 'normal'
        self.seq = original_sequence

    def call_next_button(self):
        """

        Method used as the function of the Next button
        in order to show step by step the process of a process

        """
        try:
            click = next(self.bwt_creator)                                 # First, the try clause (the statement(s) between the try and except keywords) is executed.
            self.text.delete(1.0, "end")                                   # If no exception occurs, the except clause is skipped and execution of the try statement is finished.
            self.text.insert("end", click)                                 # If an exception occurs during execution of the try clause, the rest of the clause is skipped. 
        # if no handler is found, it is an unhandled exception and execution stops 
        except StopIteration :
            click = self.seq
            self.text.delete(1.0, "end")
            self.text.insert("end", "Generated sequence: " + click)
            self.bwt_button['state'] = 'disabled'
            self.reverse_button['state'] = 'normal'
            self.compress_button['state'] = 'normal'
            self.decompress_button['state'] = 'disabled'
    
    def call_preview_button(self):
        """

        Method used as the function of the Preview button
        in order to show the last step of a process 

        """
        try:
            click = list(self.bwt_creator)[-1]                          #selects the last generated sequence
            self.text.delete(1.0, "end")
            self.text.insert("end","The last sequence: " + click)
            self.seq = click                                           #updates self.seq as click 
        except IndexError :                                            #for trying to access an index that is invalid
            self.bwt_button['state'] = 'disabled'
            self.reverse_button['state'] = 'normal'
            self.compress_button['state'] = 'normal'
            self.decompress_button['state'] = 'disabled'
            self.next_button['state'] = 'normal'

    def call_save(self):
        """

        Method used as the function of the Save button
        in order to show save the results in a txt file by choosing your directory

        """
        f = filedialog.asksaveasfilename()
        file = open(f, 'w')
        file.write(self.seq)
        if self.reversed_dictionnary :
            file.write(str(self.reversed_dictionnary))
        messagebox.showinfo('Saving',"Your results are now saved")
        file.close()

def launchApp():
    """
    launches the interface
    """
    view = Interface()
    view.mainloop()
    
if __name__=='__main__':
    launchApp()