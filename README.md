<img src="icon.png" align="right" />

# The Burrows-Wheeler Transform & Huffman compression Project

- A Python program for ***The Burrows-Wheeler Transform (BWT)*** and the ***Huffman compression*** on DNA sequences.
- Hosted on [Gitlab]

## Introduction

The most important application of BWT is found in biological sciences where genomes(long strings written in A, C, T, G alphabets) that don't have many runs but they do have many repeats.  

The idea of the BWT is to build an array whose rows are all cyclic shifts of the input string in dictionary order and return the last column of the array that tends to have long runs of identical characters. 

The benefit of this is that once the characters have been clustered together, they effectively have an ordering, which can make our string more compressible for other algorithms like run-length encoding and Huffman Coding.


### Scripts
- b_w_t.py
- tree.py
- huffman.py
- interface.py "Main File"

## Installation

- You can install the package from the source code hosted on github.

```bash
git clone git@gitlab.com:samerelkhoury/algorith-project.git
```

## Getting started

## Prerequisites

### libraries to install :
- tkinter 
- ttkthemes

Using the following commands 
```bash
pip install tkinter 
pip install ttkthemes
```


## Guide 

## Run Program 

- make sure all the downloaded files are in the same directory
- click run on the **interface.py** file to launch your program

### Instructions step by step on how to use this program

**COMPRESSION :**
- Step 1 : click on the Open button and choose a text file containing your DNA sequence.
- Step 2 : click on the BWT sequence button to do the Burrows-Wheeler Transformation on your sequence.
- Step 3 : click on the Next button to walk you through the process step by step.
- Step 3': click on the Preview button to get the final result of the process.
- Step 4 : click on the Compress button to get the binary and the ascii version of your sequence.
- Step 5 : click on the Save button and name your output file + **.txt extension.**

**DECOMPRESSION :**
- Step 1 : click on the Open button and choose your compressed file containing your DNA sequence.
- Step 2 : click on the Decompress button to get your main DNA sequence.
- Step 3 : click on the Reverse BWT button to do the reverse Burrows-Wheeler Transformation on your sequence.
- Step 3': click on the Next button to walk you through the process step by step.
- Step 4 : click on the Preview button to get the final result of the process.
- Step 5 : click on the Save button and name your output file + **.txt extension**

## Versions

- Python 3.8.10
- Tkinter 8.6

## Tips 

In case of any errors please use the test3.txt file

## Authors 

Samer El Khoury 
- email : <samer.EL-KHOURY@etu.univ-amu.fr>

## Now You Are Ready To Go !
