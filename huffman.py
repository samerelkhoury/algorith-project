from tree import Tree
"""
Huffman creator class that uses the binary tree
with all the conversion methods
"""
__author__ = 'Samer El Khoury'

class Huffman_creator :
    """
    Huffman Class creator

    Returns
    -------
    None

    """
    def __init__(self):
        self.reconstructed_sequence = None

    @staticmethod
    def encode_huffman(reconstructed_sequence):
        """
        Method that calls the Tree Class to create a useful dictionnary and to do 
        the binary to ascii conversion, ascii to binary , and binary to the original sequence

        Input
        -------
        reconstructed_sequence: str 
                               The result of the Burrows-Wheeler Algorithm

        Returns
        -------
        encoded_character: 
                        The huffman code of the DNA sequence

        coding_dic: dic
                    The main dictionnary containing the binary code for each character, with the letters as keys and binary code as values

        reversed_dic: dic
                    The reversed dictionnary containing the binary code for each character

        """
        # Get a dictionary that stores the codes
        tree1 = Tree(reconstructed_sequence)
        coding_dic = tree1.leaf_paths
    
        # Make an encoded string
        encoded_character = ""
        # Replace each character by it Huffman Code.
        for char in reconstructed_sequence:
            encoded_character = encoded_character + str(coding_dic[char])

        reversed_dic = {}
        for lettre, path in coding_dic.items():    #reverses the keys and values of the main dictionnary
            reversed_dic[path] = lettre

        return encoded_character,reversed_dic,coding_dic


    @staticmethod
    def binary_to_ASCII(encoded_character,reversed_dic):
        """
        Method to convert the binary sequence to an ascii code 

        Input
        -------
        encoded_character: 
                         The huffman code of the DNA sequence

        reversed_dic: dic
                    The reversed dictionnary containing the binary code for each character

        Returns
        -------
        ascii_code : 
                    The ascii code generated 

        reversed_dic : dic
                     The reversed dictionnary containing the binary code for each character
                       
        """
        ascii_code =""
        for bit in range(0,len(encoded_character),8):
            if bit+8 <= len(encoded_character):
                bits = encoded_character[bit:bit+8] 
                code = int(bits, 2)
            else : 
                bits = encoded_character[bit:len(encoded_character)]
                code = int(bits, 2)
            ascii_code += chr(code)
        reversed_dic[chr(code)] = len(bits)
        return ascii_code,reversed_dic

    @staticmethod
    def ASCII_to_binary(ascii_code,reversed_dic):
        """
        Method to replace the ASCII sequence with the Binary sequence

         Input
        -------
        to_binary: 
                 The binary version of the DNA sequence

        reversed_dic: dic
                    The reversed dictionnary containing the binary code for each character

        Returns
        -------
        to_binary :
                  The binary sequence obtained from the ascii code 
                       
        """
        to_binary = ""
        for i,j in enumerate(ascii_code) :               #to get a counter and the value from the iterable at the same time
            code = ord(j)                                #converts a character into its Unicode cod
            if i != len(ascii_code)-1: 
                to_binary += str('{:0>8b}'.format(code))   #converts decimal to 8 bit binary
            else :
                last_character_length = list(reversed_dic.values())[-1]           # if the last character isn't 8 bits it adds a new key and value at the end of the dictionnary
                to_binary += str(format(code,'b')).zfill(int(last_character_length))   #it Fills the string with zeros until it is the last characters length
        return to_binary

    @staticmethod
    def binary_to_original_sequence(to_binary,reversed_dic):
        """
        Method to replace the binary sequence with the original characters of the DNA sequence

         Input
        -------
        to_binary: 
                 the binary version of the DNA sequence

        reversed_dic: dic
                    the reversed dictionnary containing the binary code for each character

        Returns
        -------
        original sequence : str
                            the original DNA sequence 
                       
        """
        original_sequence = ""
        step_1 = 1                         #creates a small window to read the binary code 
        i = 0
        while i + step_1 <= len(to_binary):
            if to_binary[i:i + step_1] in reversed_dic.keys():
                original_sequence += reversed_dic[to_binary[i:i + step_1]]  #if the corresponding binary code of the character is found
                i = i + step_1                                              # the binary code will be replace by it's character
                step_1 = 1                                                  # and the window will continue to the next part of the binary sequence
            else :
                step_1 += 1 
        return original_sequence
